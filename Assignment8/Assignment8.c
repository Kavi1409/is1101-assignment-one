#include <stdio.h>

struct student{
	int stno;
	char fname[50];
	char subject[50];
	float marks;
}

s[10];

int main (){

	int i, x;
	printf("Enter the number of student records (min 5 students & max 10 students): ");
	scanf("%d",&x);

	//checks if the entered number is in the given range
	
	if( x < 11 && x > 4){
		printf("---Enter student details---\n\n");
		
		//enter details

		for(i=0; i<x;i++){
			s[i].stno = i+1;
			printf("\nRecord No: %d\n",s[i].stno);
			printf("Enter First Name: ");
			scanf("%s", s[i].fname);
			printf("Enter Subject: ");
			scanf("%s",s[i].subject);
			printf("Enter marks: ");
			scanf("%f", &s[i].marks); 
		}

		printf("\n ---Printed details of Students---\n\n");

		//print details

		for(i=0; i<x; i++){
			printf("Record No: %d\n", i+1);
			printf("Student Name: ");
			puts(s[i].fname);
			printf("Subject: ");
			puts(s[i].subject);
			printf("Marks: %.2f", s[i].marks);
			printf("\n---\n");
		} 
	}
	
	else 
		printf("Invalid number of records!\n");

	return 0;
}
