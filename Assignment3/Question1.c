//a c program to calculate the sum of all given positive integers
//until user enters 0 or a negative!
//
#include <stdio.h>

int main (){

	int num=0, sum=0;


	do{
		printf("Enter number: ");
		scanf("%d",&num);
		if (num<=0) break;
		sum=sum+num;
	}
	while (num>0);

	printf("Total of the numbers is %d\n", sum);

	return 0;
}

