// a c program to find the factors of a given number
//
#include <stdio.h>

int main (){

	int num, i;
	printf("Enter positive integer > ");
	scanf("%d",&num);

	for (i=1; i<=num ; i++){
		if (num%i == 0) printf("%d, ",i);
	}
	printf("\n");

	return 0;
}

