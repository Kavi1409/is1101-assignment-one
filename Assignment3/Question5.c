// a c program to print multiplication tables from i to n,
// by a given integer value n

#include <stdio.h>
int main (){

	int n,i,j,t;
	printf("Enter a number to create tables: ");
	scanf("%d",&n);

	for (i=1; i<=n; i++){
		for (t=1 ; t<=10; t++){
			j=i*t;
			printf("%d x %d = %d\n", i, t, j);}

		printf("------------------\n");
	}
	
	return 0;
}


