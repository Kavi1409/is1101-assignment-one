// c program to check if a number is negative, positive or zero
//
#include <stdio.h>

int main (){
	int x;
	printf("Enter an integer > ");
	scanf("%d",&x);

	if (x==0)
		printf("ZERO!\n");
	else if (x>0)
		printf("POSITIVE!\n");
	else
		printf("NEGATIVE!\n");

	return 0;

}
