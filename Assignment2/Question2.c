// a c program to find out if a number is odd or even
//
#include <stdio.h>

int main (){

	int num, r;

	printf("Enter an Integer > ");
	scanf("%d",&num);

	r=num%2;

	(r==0)? printf("Even\n") : printf("Odd\n");

	return 0;
}

