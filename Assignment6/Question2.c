#include <stdio.h>

int fibonacciSeq(int);


int main(){
    
    int x,y,z;
    x=0;
    printf("Enter number to print Fibonacci Sequence: ");
    scanf("%d",&x);
    printf("Fibonacci Sequence for %d is:\n",x);
    for(y=0; y<=x; y++){
        z=fibonacciSeq(y);
        printf("%d\n",z);
    }
    
	return 0;
}

//Recursive function to calculate the fibonacci sequence

int fibonacciSeq(int n){
		if (n==0) return 0;
		else if (n==1) return 1;
		else return fibonacciSeq(n-1)+ fibonacciSeq(n-2);
}



