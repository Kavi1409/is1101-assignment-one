#include <stdio.h>

int pattern(int);

/*********************/

int main(){
    
    int n;
    printf("Enter Number to print the pattern: ");
    scanf("%d",&n);
    printf("Number Pattern:\n");
	pattern(n);
	return 0;
}

/*********************/

int pattern(int n){
	int i,j;
	i=n;
	if (i==0)
		return 0;
	else {	
          pattern(n-1);
          
          for(j=i; j>=1; j--)
		    printf("%d",j);
		  
		  printf("\n");
	    }
}
			



