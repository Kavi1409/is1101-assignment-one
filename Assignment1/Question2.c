/*a C program to read the radius r and compute the area of a disk*/

#include <stdio.h>

int main (){

	float radius,pi,area;

	pi=3.14;

	printf("Give me the radius of the disk:");
	scanf("%f",&radius);

	area=pi*(radius*radius);

	printf("The area of the disk is:%.3f\n",area);

	return 0;
}
