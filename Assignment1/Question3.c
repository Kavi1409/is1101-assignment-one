/* a C program to read two integers and swap it */

#include <stdio.h>

int main (){

	int x,y,z;

	printf("Give me two integers and I will swap them!\n");
	printf("Number A:");
	scanf("%d",&x);
	printf("Number B:");
	scanf("%d",&y);

	z=x;
	x=y;
	y=z;

	printf("Number A is now Number B -> %d\n",x);
	printf("Number B is now Number A -> %d\n",y);

	return 0;
}
