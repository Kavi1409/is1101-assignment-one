
/* a C program to read and multiply two numbers */

#include <stdio.h>

int main (){

	float x,y,answer;

	printf("Give me two numbers to multiply:\n");
	scanf("%f %f",&x,&y);

	answer=x*y;

	printf("%.2f X %.2f = %.2f\n",x,y,answer);

	return 0;
}
