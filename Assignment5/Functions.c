#include <stdio.h>

int attend(int);
int income(int, int);
int expense(int);
int profit(int, int);

/****************************************/

int attend(int a){
	int people=120;
	int n= ((a-15)/5) * 20;
	people = people - n;
	return people;
}

int income(int x, int y){
	return x*y;
}
int expense(int z){
	return 500 + (3 * attend(z));
	
}

int profit (int a, int b){
	int p;
	p= a - expense(b);
	return p;
}

/**************************************/

int main(){
	int i, a, n;
	printf("\n");
	printf("Price and Average Attendance has been calculated by using multiples of 5!\n");
	for (i=0; i<50; i=i+5){
		a=attend(i);
		n=profit(income(i, a), i);
		printf("-----------------------------------------------------------\n");
		printf("Price: Rs.%d | Attendance: %d | Profit: Rs.%d\n",i,a,n);
	}	
	
	printf("-----------------------------------------------------------\n");
	printf("\n");
	return 0;
}

