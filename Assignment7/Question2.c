//c program to find the frequency of a given character in a sting

#include <stdio.h>
#include <string.h>
#define SIZE 200

int main(){

	char string[SIZE];
	char x;
	int count=0;
	
	printf("Enter your sentence:\n");
	fgets(string,SIZE,stdin);

	printf("Enter letter to find the frequency of: ");
	scanf("%c",&x);

	for(int i=0; i<strlen(string); i++){
		if (x == string[i]) count=count+1;
	}

	printf("The frequency of %c is %d!\n", x, count);
	return 0;
}


