//add and multiply two matrices

#include <stdio.h>
#define R 3 //number of rows
#define C 3 //number of columns

void ReadMatrix(int [R][C]);
void PrintMatrix(int [R][C]);

/*****************************************************/
void ReadMatrix(int m[R][C]){
	printf("Enter data for %d X %d matrix: ", R, C);
	for(int i=0; i<R; i++){
		for(int j=0; j<C;j++)	scanf("%d",&m[i][j]); 
	}
}

void PrintMatrix(int m[R][C]){
	for(int i=0; i<R; i++){
		for(int j=0; j<C;j++)	printf("%d ",m[i][j]);
	       printf("\n");	
	}
}
/****************************************************/

int main(){
	
	int m1[R][C], m2[R][C], add[R][C], multi[R][C];
	int sum=0;

	ReadMatrix(m1);
	PrintMatrix(m1);
	printf("-----------------------\n");
	ReadMatrix(m2);
	PrintMatrix(m2);
	
	printf("-----------------------\n");

	//adding the two matrices
	for(int i=0; i<R; i++)
		for(int j=0; j<C; j++) add[i][j]= m1[i][j]+m2[i][j];

	printf("Sum of the two matrices: \n");

	for(int i=0; i<R; i++){
		for(int j=0; j<C; j++)	printf("%d ", add[i][j]);
		printf("\n");
	}

	printf("-----------------------\n");

	//multiplying the two matrices
	for(int i=0; i<R; i++){
		for(int j=0; j<C; j++){
		       for (int k=0; k<R; k++){
			       sum=sum +  m1[i][k] * m2[k][j];
		       }
		       multi[i][j]=sum;
		       sum=0;
		}
	}

	printf("Multiplication of the two matrices: \n");

	for(int i=0; i<R; i++){
		for(int j=0; j<C; j++)	printf("%d ", multi[i][j]);
		printf("\n");
	}

	printf("-----------------------\n");
	return 0;
}


