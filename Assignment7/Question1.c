//c program to reverse a sentence

#include <stdio.h>
#include <string.h>
#define SIZE 100

int main(){

	char s[SIZE];
	printf("Enter sentence: ");
	fgets(s, SIZE, stdin);
	int len=strlen(s);

	for (int i=len; i>=0; i--) printf ("%c", s[i]);
	printf("\n");

	return 0;
}
